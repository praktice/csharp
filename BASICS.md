# C# Basics

- [C# Basics](#c-basics)
  - [Data Types](#data-types)
  - [Simple Output](#simple-output)
  - [Keywords](#keywords)
  - [Contextual` Keywords](#contextual-keywords)

## Data Types

The **U** Prefix stands for **unsigned** which means positive only.

| C# Type   | .NET Type        |
| --------- | ---------------- |
| `bool`    | `System.Boolean` |
| `byte`    | `System.Byte`    |
| `sbyte`   | `System.SByte`   |
| `char`    | `System.Char`    |
| `decimal` | `System.Decimal` |
| `double`  | `System.Double`  |
| `enum`    |                  |
| `float`   | `System.Single`  |
| `int`     | `System.Int32`   |
| `uint`    | `System.Uint32`  |
| `long`    | `System.Int64`   |
| `ulong`   | `System.UInt64`  |
| `object`  | `System.Object`  |
| `short`   | `System.Int16`   |
| `ushort`  | `System.UInt16`  |
| `string`  | `System.String`  |
| `struct`  |                  |
| `void`    |                  |
| `var`     |                  |

## Simple Output

```cs
Console.WriteLine("Hello {0}, today is {1}", "Jesse", "Nice");

# Same as Above
string name = "Jesse";
string day = "Nice";
Console.WriteLine($"Hello, {name}, today is {day}.");
```

## Keywords

|             |              |             |             |
| ----------- | ------------ | ----------- | ----------- |
| `abstract`  | `as`         | `base`      | `bool`      |
| `break`     | `byte`       | `case`      | `catch`     |
| `char`      | `checked`    | `class`     | `const`     |
| `continue`  | `decimal`    | `default`   | `delegate`  |
| `do`        | `double`     | `else`      | `enum`      |
| `event`     | `explicit`   | `extern`    | `false`     |
| `finally`   | `fixed`      | `float`     | `for`       |
| `foreach`   | `goto`       | `if`        | `implicit`  |
| `in`        | `int`        | `interface` | `internal`  |
| `is`        | `lock`       | `long`      | `namespace` |
| `new`       | `null`       | `object`    | `operator`  |
| `out`       | `override`   | `params`    | `private`   |
| `protected` | `public`     | `readonly`  | `ref`       |
| `return`    | `sbyte`      | `sealed`    | `short`     |
| `sizeof`    | `stackalloc` | `static`    | `string`    |
| `struct`    | `switch`     | `this`      | `throw`     |
| `true`      | `try`        | `typeof`    | `uint`      |
| `ulong`     | `unchecked`  | `unsafe`    | `ushort`    |
| `using`     | `using`      | `static`    | `virtual`   |
| `void`      | `volatile`   | `while`     |

## Contextual` Keywords

|                                   |                        |
| --------------------------------- | ---------------------- |
| `add`                             | `alias`                | `ascending` |
| `async`                           | `await`                | `by` |
| `descending`                      | `dynamic`              | `equals` |
| `from`                            | `get`                  | `global` |
| `group`                           | `into`                 | `join` |
| `let`                             | `nameof`               | `on` |
| `orderby`                         | `partial` (type)       | `partial` (method) |
| `remove`                          | `select`               | `set` |
| `value`                           | `var`                  | `when` | (filter condition) |
| `where` (generic type constraint) | `where` (query clause) | `yield` |
