# CSharp and CLI

- [CSharp and CLI](#CSharp-and-CLI)
  - [Commands: New](#Commands-New)
  - [Commands: Compile](#Commands-Compile)
  - [Commands: Main](#Commands-Main)
  - [Commands: Bundled Tools](#Commands-Bundled-Tools)

---

> Make sure to always create a **new folder**, then run a command. New Dotnet File

```sh
mkdir <project-name>
dotnet new <option>
```

## Commands: New

| CMD             | Desc |
| --------------- | ---- |
| `console`       |      |
| `mstest`        |      |
| `page`          |      |
| `web`           |      |
| `webapp`        |      |
| `react|angular` |      |
| `webapi`        |      |
| `sln`           |      |
| `webconfig`     |      |
| `globaljson`    |      |

## Commands: Compile

| CMD      | Desc            |
| -------- | --------------- |
| `csc`    | CSharp Compiled |
| `csharp` | CSharp REPL     |

## Commands: Main

| CMD            | Desc                                                              |
| -------------- | ----------------------------------------------------------------- |
| `add`          | Add a package or reference to a .NET project.                     |
| `build`        | Build a .NET project.                                             |
| `build-server` | Interact with servers started by a build.                         |
| `clean`        | Clean build outputs of a .NET project.                            |
| `help`         | Show command line help.                                           |
| `list`         | List project references of a .NET project.                        |
| `migrate`      | Migrate a project.json project to an MSBuild project.             |
| `msbuild`      | Run Microsoft Build Engine (MSBuild) commands.                    |
| `new`          | Create a new .NET project or file.                                |
| `nuget`        | Provides additional NuGet commands.                               |
| `pack`         | Create a NuGet package.                                           |
| `publish`      | Publish a .NET project for deployment.                            |
| `remove`       | Remove a package or reference from a .NET project.                |
| `restore`      | Restore dependencies specified in a .NET project.                 |
| `run`          | Build and run a .NET project output.                              |
| `sln`          | Modify Visual Studio solution files.                              |
| `store`        | Store the specified assemblies in the runtime package store.      |
| `test`         | Run unit tests using the test runner specified in a .NET project. |
| `tool`         | Install or manage tools that extend the .NET experience.          |
| `vstest`       | Run Microsoft Test Engine (VSTest) commands.                      |

## Commands: Bundled Tools

| CMD            | Desc                                                        |
| -------------- | ----------------------------------------------------------- |
| `dev-certs`    | Create and manage development certificates.                 |
| `ef`           | Entity Framework Core command-line tools.                   |
| `fsi`          | Start F# Interactive / execute F# scripts.                  |
| `sql-cache`    | SQL Server cache command-line tools.                        |
| `user-secrets` | Manage development user secrets.                            |
| `watch`        | Start a file watcher that runs a command when files change. |

